from abc import abstractmethod
import pandas as pd


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


class BaseSearchService:
    DOCS_COLUMNS = ['document', 'key', 'key_md5']

    @abstractmethod
    def get_search_data(self, search_text, user_data=None, limit=10) -> pd.DataFrame:
        pass
