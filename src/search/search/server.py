from flask import Flask, request
from search_shard.service import Service


def health():
    return 'OK'


class Server(Flask):
    def __init__(self, name: str, service: Service):
        super().__init__(name)
        self._service = service
        urls = [
            ('/health', health, {}, ['GET']),
            ('/search', self.search, {}, ['POST']),
        ]
        for url in urls:
            self.add_url_rule(url[0], url[1].__name__, url[1], **url[2], methods=url[3])

    def search(self):
        search_text = request.args.get('search_text')
        user_data = request.get_json(force=True, silent=True)
        return {
            'search_results': self._service.get_search_data(search_text, user_data)[self._service.DOCS_COLUMNS].to_dict(
                orient='records')
        }

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
