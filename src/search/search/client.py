import requests


class Client:
    def __init__(self, host: str, port: int):
        self._host = host
        self._port = port

    def get_search_data(self, search_text: str, user_data, limit: int):
        r = requests.post(f"http://{self._host}:{self._port}/search", params={
            'search_text': search_text,
            'limit': limit
        }, json=user_data)

        return r.json()
