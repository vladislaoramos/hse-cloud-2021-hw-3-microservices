from search_shard.service import Service as SimpleSearchService
from search_shard.client import Client
from typing import List
import pandas as pd


class Service(SimpleSearchService):
    def __init__(self, shards: List[str]):
        self._shard_clients = [Client(host, 8000) for host in shards]

    def get_search_data(self, search_text, user_data=None, limit=10) -> pd.DataFrame:
        shards_responses = []
        for shard_client in self._shard_clients:
            shards_responses.append(
                pd.DataFrame(shard_client.get_search_data(search_text, user_data)["search_results"]))
        self._data = pd.concat(shards_responses)  # possible data race in case of multi thread/async usage
        self._data.reset_index(inplace=True, drop=True)
        assert self._data.index.is_unique

        return super().get_search_data(search_text, user_data, limit)
