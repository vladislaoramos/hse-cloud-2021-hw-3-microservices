from flask import Flask, request
from users.service import Service


class Server(Flask):
    def __init__(self, name: str, service: Service):
        super().__init__(name)
        self._service = service
        urls = [
            ('/health', self.health, {}, ['GET']),
            ('/user_data', self.get_user_data, {}, ['GET']),
        ]
        for url in urls:
            self.add_url_rule(url[0], url[1].__name__, url[1], **url[2], methods=url[3])

    def get_user_data(self):
        user_id = int(request.args.get('user_id'))
        return self._service.get_user_data(user_id)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)

    def health(self):
        return 'OK'