from flask import Flask, request
from metasearch.service import Service


def health():
    return 'OK'


class Server(Flask):
    def __init__(self, name: str, service: Service):
        super().__init__(name)
        self._service = service
        urls = [
            ('/health', health, {}, ['GET']),
            ('/search', self.search, {}, ['GET']),
        ]
        for url in urls:
            self.add_url_rule(url[0], url[1].__name__, url[1], **url[2], methods=url[3])

    def search(self):
        search_text = request.args.get('text')
        user_id = int(request.args.get('user_id'))
        return self._service.search(search_text, user_id)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
