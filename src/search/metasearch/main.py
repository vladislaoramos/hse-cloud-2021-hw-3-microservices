from metasearch.server import Server
from metasearch.service import Service
# from settings import SEARCH_HOST, USERS_HOST

import os

USERS_DATA = os.getenv("USERS_DATA")
SHARDS_DATA = [os.getenv("SHARD0_DATA"), os.getenv("SHARD1_DATA")]

METASEARCH_HOST = os.getenv("METASEARCH_HOST")
SHARDS_HOSTS = os.getenv("SHARDS_HOSTS").split(',')
SEARCH_HOST = os.getenv("SEARCH_HOST")
USERS_HOST = os.getenv("USERS_HOST")



def main():
    service = Service(SEARCH_HOST, USERS_HOST)
    server = Server('metasearch', service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
