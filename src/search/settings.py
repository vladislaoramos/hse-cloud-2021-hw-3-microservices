import os

USERS_DATA = os.getenv("USERS_DATA")
SHARDS_DATA = [os.getenv("SHARD0_DATA"), os.getenv("SHARD1_DATA")]

METASEARCH_HOST = os.getenv("METASEARCH_HOST")
SHARDS_HOSTS = os.getenv("SHARDS_HOSTS").split(',')
SEARCH_HOST = os.getenv("SEARCH_HOST")
USERS_HOST = os.getenv("USERS_HOST")
