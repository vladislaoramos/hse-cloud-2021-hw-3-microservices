from search_shard.server import Server
from search_shard.service import Service
from common.data_source import CSV
# from settings import SHARDS_DATA
# import os
import os

USERS_DATA = os.getenv("USERS_DATA")
SHARDS_DATA = [os.getenv("SHARD0_DATA"), os.getenv("SHARD1_DATA")]

METASEARCH_HOST = os.getenv("METASEARCH_HOST")
SHARDS_HOSTS = os.getenv("SHARDS_HOSTS").split(',')
SEARCH_HOST = os.getenv("SEARCH_HOST")
USERS_HOST = os.getenv("USERS_HOST")


def main():
    shard = int(os.getenv("SHARD"))
    service = Service(CSV(SHARDS_DATA[shard]))
    server = Server(f'search_shard{shard}', service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
