import time

import pytest
import requests


@pytest.fixture
def search_baseurl():
    return f'http://users:8000'


@pytest.fixture
def ensure_search_running(search_baseurl):
    while True:
        try:
            response = requests.get(search_baseurl)
            assert response.status_code == 404
            break
        except requests.exceptions.ConnectionError:
            print('Waiting for search to run...')
            time.sleep(1)


@pytest.mark.usefixtures('ensure_search_running')
def test_users_integration(search_baseurl):
    path = 'user_data'
    params = 'user_id=35'
    url = f'{search_baseurl}/{path}?{params}'
    response = requests.get(url).json()

    assert response['age'] == 56
    assert response['gender'] == 'male'
