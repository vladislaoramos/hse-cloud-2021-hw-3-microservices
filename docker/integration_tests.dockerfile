FROM python:3.9

RUN mkdir /app/
WORKDIR /app/

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY src src

ENV PYTHONPATH=./src/
CMD ["pytest", "-vs", "src/tests/"]
